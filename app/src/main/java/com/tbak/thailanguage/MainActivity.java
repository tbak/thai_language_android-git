/*
 TODO:
 Better page load finished flicker solution
 Show keyboard on startup! Why did this stop working?

 */
package com.tbak.thailanguage;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final public static String TAG = "MainActivity";
    WebView mWebView;
    EditText mSearch;
    TextView mStatus;
    boolean mGotError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStatus = (TextView) findViewById(R.id.status);
        mStatus.setVisibility(View.VISIBLE);

        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.setVisibility(View.INVISIBLE);

        //mWebView.loadData("HELLO!!!!!!!!", "text/html", "UTF-8");
        mWebView.getSettings().setJavaScriptEnabled(true);

        // Does this enable zooming?
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setLoadsImagesAutomatically(false);

        // Do we want this?
        // mWebView.setInitialScale(200);

        // This fully zooms out the text
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        mWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                // If there was an error loading the page, then show our error status message instead of the ugly
                // webview error stuff.
                if( mGotError ) {
                    return;
                }
                // Toast.makeText(MainActivity.this, "Done loading!", Toast.LENGTH_SHORT).show();
                // void(0) prevents the browser from then navigating to 'none'

                mWebView.loadUrl("javascript:document.getElementById('navtab-underline').style.display='none';void(0);");

                // Remove left column stuff
                mWebView.loadUrl("javascript:document.getElementById('tl-left-column').style.display='none';void(0);");
                mWebView.loadUrl("javascript:document.getElementById('tl-content').style.marginLeft='0px';void(0);");

                // Remove top "thai-language" image strip
                mWebView.loadUrl("javascript:document.getElementsByTagName('table')[0].style.display='none';void(0)");

                // Remove top tabs row
                mWebView.loadUrl("javascript:document.getElementById('tl-content').getElementsByTagName('div')[0].style.display='none';void(0)");

                // Remove chunk of text above the search results
                mWebView.loadUrl("javascript:document.getElementById('old-content').getElementsByTagName('table')[0].style.display='none';void(0)");

                mWebView.loadUrl("javascript:document.getElementById('old-content').style.marginTop='0px';void(0)");


                // Make the webview visible after a short while so that the above javascript has time to run.
                // Otherwise you will see the full page, and then the columns disappear
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Log.d(TAG, "Setting VISIBLE!");
                        mWebView.setVisibility(View.VISIBLE);
                        mStatus.setVisibility(View.INVISIBLE);
                    }
                }, 200);

            }

            @Override
            public void onPageStarted (WebView view, String url, Bitmap favicon) {
                mGotError = false;
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                // Handle the error
                // TB - This gives an unhelpful error message and description if internet is not working.
                // Best to just show the default webview error stuff!
                /*
                mStatus.setText("Error " + errorCode + "\n" + description);
                mGotError = true;
                */
            }

        } );

        ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.action_search);
        mSearch = (EditText) actionBar.getCustomView().findViewById(R.id.edit_search);
        mSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    // Toast.makeText(MainActivity.this, "Searching: " + mSearch.getText().toString(), Toast.LENGTH_SHORT).show();
                    mWebView.setVisibility(View.INVISIBLE);
                    mStatus.setVisibility(View.VISIBLE);
                    mStatus.setText("Searching: " + mSearch.getText().toString());

                    //Log.d(TAG, "Setting INVISIBLE!");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mSearch.getWindowToken(), 0);

                    // TB TODO - This returns a bunch of XML. Eventually we'll need to properly parse that. :(
                    // mWebView.loadUrl("http://thai-language.com/xml/FullSearch?fmt=0&input=" + search.getText().toString());

                    String url = "http://thai-language.com/default.aspx";
                    mWebView.loadUrl(url + "?search=" + mSearch.getText().toString());
                    handled = true;
                }

                return handled;
            }
        });
        // actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        // actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        setFocusToSearchBar();
    }

    void setFocusToSearchBar() {
        mSearch.requestFocus();

        // This is needed to make the keyboard pop up afte hitting the clear button (but does not show the keyboard when the app first starts).
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mSearch, InputMethodManager.SHOW_IMPLICIT);

        // This is needed to make the keyboard appear when the app first starts. Why do I need separate calls to do the same thing???
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch( item.getItemId() ) {
            case R.id.action_about:
                // Toast.makeText(MainActivity.this, "about", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle("Thai-language.com Android App");
                builder.setMessage("Enjoy!\n\nTom Bak\ntom@thomasbak.com");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });

                AlertDialog alert = builder.create();
                alert.show();
                break;
            case R.id.action_clear:
                // Toast.makeText(MainActivity.this, "clear", Toast.LENGTH_SHORT).show();
                mSearch.getText().clear();
                setFocusToSearchBar();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    // TB TODO - Can't I just use onBackButtonPressed????
    @Override
    //public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Make the back key go back in the webview instead of exiting out of the app!
        //if(keyCode == KeyEvent.KEYCODE_BACK) {
    public void onBackPressed() {
            if( mWebView.canGoBack() ) {
                mWebView.goBack();
            }
            else {
                // Show a dialog box asking if you want to exit, like the reddit app does?
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle("Confirm");
                builder.setMessage("Are you sure you want to exit?");

                builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // finish();
                        MainActivity.super.onBackPressed();
                    }

                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
        //return super.onKeyDown(keyCode, event);
    //}
}
